package pt.uevora;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MyCalculatorTest {

    @BeforeEach
    public void setUp(){
        
    }

    @AfterEach
    public void down(){
    }

    @Test
    public void test() throws Exception {
      MyCalculator cal = new MyCalculator();

      assertEquals(6,cal.sum("4","2"));
      assertEquals(0,cal.sum("0","0"));
      assertEquals(-1,cal.sum("3","-4"));
      assertEquals(-5,cal.sum("-2","-3"));
      assertEquals(-1,cal.sub("0","1"));
      assertEquals(0,cal.sub("0","0"));
      assertEquals(10,cal.sub("15","5"));
      assertEquals(-5,cal.sub("10","15"));
      assertEquals(56,cal.mul("8","7"));
      assertEquals(0,cal.mul("40","0"));
      assertEquals(0,cal.mul("0","0"));
      assertEquals(-2,cal.mul("1","-2"));
      assertEquals(4,cal.mul("-2","-2"));
      assertEquals(2,cal.div("4","2"));
      assertEquals(-3,cal.div("3","-1"));
      assertEquals(6,cal.div("-18","-3"));

    }

}